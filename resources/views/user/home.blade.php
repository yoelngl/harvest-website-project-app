@extends('layouts.master')

@section('title')
Home
@endsection

@section('content')
<section class="company_banner_area">
	<div class="parallax-effect" style="background: url( {{ asset('assets/img/new/company.jpg') }} ):"></div>
	<div class="container">
		<div class="company_banner_content">
			<h6>January 10, 2019 / New York City </h6>
			<h2>HARVEST CHURCH Indonesia</h2>
		</div>
	</div>
</section>
<section class="saas_features_area_three bg_color sec_pad">
	<div class="container">
		<div class="sec_title text-center mb_70 wow fadeInUp" data-wow-delay="0.3s">
			<h2 class="f_p f_size_30 l_height50 f_600 t_color">Visi dan Motto</h2>
			<p class="f_400 f_size_16">Skive off mush victoria sponge super lavatory it's all gone to pot<br> knees up fanny around vagabond</p>
		</div>
		<div class="row mb_30 new_service">
			<div class="col-lg-4 col-sm-6">
				<div class="saas_features_item text-center wow fadeInUp" data-wow-delay="0.3s">
					<div class="number">1</div>
					<div class="separator"></div>
					<div class="new_service_content">
						<img src="{{ asset('assets/img/home-10/icon1.png') }}" alt="">
						<h4 class="f_size_20 f_p t_color f_500">BERTUMBUH</h4>
						<p class="f_400 f_size_15 mb-0">Tosser blag bubble and squeak up the duff A bit of how's your father blatant morish char cuppa.!</p>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-sm-6">
				<div class="saas_features_item text-center wow fadeInUp" data-wow-delay="0.5s">
					<div class="number">2</div>
					<div class="separator"></div>
					<div class="new_service_content">
						<img src="{{asset('assets/img/home-10/icon2.png') }}" alt="">
						<h4 class="f_size_20 f_p t_color f_500">BERMASYARAKAT</h4>
						<p class="f_400 f_size_15 mb-0">Tosser blag bubble and squeak up the duff A bit of how's your father blatant morish char cuppa.!</p>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-sm-6">
				<div class="saas_features_item text-center wow fadeInUp" data-wow-delay="0.7s">
					<div class="number">3</div>
					<div class="separator"></div>
					<div class="new_service_content">
						<img src="{{ asset('assets/img/home-10/icon3.png') }}" alt="">
						<h4 class="f_size_20 f_p t_color f_500">BERBUDAYA</h4>
						<p class="f_400 f_size_15 mb-0">Tosser blag bubble and squeak up the duff A bit of how's your father blatant morish char cuppa.!</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row mb_30 new_service">
			<div class="col-lg-4 col-sm-6">
				<div class="saas_features_item text-center wow fadeInUp" data-wow-delay="0.3s">
					<div class="number">4</div>
					<div class="separator"></div>
					<div class="new_service_content">
						<img src="{{ asset('assets/img/home-10/icon1.png') }}" alt="">
						<h4 class="f_size_20 f_p t_color f_500">BANGUN GEREJA LOKAL YANG KUAT</h4>
						<p class="f_400 f_size_15 mb-0">Tosser blag bubble and squeak up the duff A bit of how's your father blatant morish char cuppa.!</p>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-sm-6">
				<div class="saas_features_item text-center wow fadeInUp" data-wow-delay="0.5s">
					<div class="number">5</div>
					<div class="separator"></div>
					<div class="new_service_content">
						<img src="{{asset('assets/img/home-10/icon2.png') }}" alt="">
						<h4 class="f_size_20 f_p t_color f_500">BERDAMPAK PADA MASYARAKAT</h4>
						<p class="f_400 f_size_15 mb-0">Tosser blag bubble and squeak up the duff A bit of how's your father blatant morish char cuppa.!</p>
					</div>
				</div>
			</div>

		</div>

	</div>
</section>

<div class="container" >
		<br>
		<div class="sec_title text-center mb_70 wow fadeInUp" data-wow-delay="0.3s">
			<h2 class="f_p f_size_30 l_height50 f_600 t_color"><section> Tentang Gereja Kami</section>  </h2>
			<p class="f_400 f_size_16">Skive off mush victoria sponge super lavatory it's all gone to pot<br> knees up fanny around vagabond</p>
		</div>
		<section class="perfect_solution_area bg_color flex-row-reverse">
			<div class="col-lg-6 perfect_solution_right">
				<div class="bg_img parallax-effect"></div>
			</div>
			<div class="col-lg-6 perfect_solution_left">
				<div class="per_solution_content">
					<h2>Harvest Medan</h2>
					<p>My lady bits and bobs cup of tea bubble and squeak brolly Harry cras in my flat the little rotter, bite your arm off cobblers plastered spend a penny bleeding.!</p>
					<a href="#" class="btn_three btn_hover">Try it Free</a>
					<a href="#" class="btn_three btn_six btn_hover">Get a Demo</a>
				</div>
			</div>
		</section>
		<section class="perfect_solution_area">
			<div class="col-lg-6 perfect_solution_right">
				<div class="bg_img bg_two parallax-effect"></div>
			</div>
			<div class="col-lg-6 perfect_solution_left">
				<div class="per_solution_content per_solution_content_two">
					<h2>Harvest Bukit Doa</h2>
					<p>My lady bits and bobs cup of tea bubble and squeak brolly Harry cras in my flat the little rotter, bite your arm off cobblers plastered spend a penny bleeding.!</p>
					<a href="#" class="btn_three btn_hover">Try it Free</a>
					<a href="#" class="btn_three btn_six btn_hover">Get a Demo</a>
				</div>
			</div>
		</section>
		<section class="perfect_solution_area bg_color flex-row-reverse">
			<div class="col-lg-6 perfect_solution_right">
				<div class="bg_img parallax-effect"></div>
			</div>
			<div class="col-lg-6 perfect_solution_left">
				<div class="per_solution_content">
					<h2>Harvest Binjai</h2>
					<p>My lady bits and bobs cup of tea bubble and squeak brolly Harry cras in my flat the little rotter, bite your arm off cobblers plastered spend a penny bleeding.!</p>
					<a href="#" class="btn_three btn_hover">Try it Free</a>
					<a href="#" class="btn_three btn_six btn_hover">Get a Demo</a>
				</div>
			</div>
		</section>
		<section class="perfect_solution_area">
			<div class="col-lg-6 perfect_solution_right">
				<div class="bg_img bg_two parallax-effect"></div>
			</div>
			<div class="col-lg-6 perfect_solution_left">
				<div class="per_solution_content per_solution_content_two">
					<h2>Harvest Porsea</h2>
					<p>My lady bits and bobs cup of tea bubble and squeak brolly Harry cras in my flat the little rotter, bite your arm off cobblers plastered spend a penny bleeding.!</p>
					<a href="#" class="btn_three btn_hover">Try it Free</a>
					<a href="#" class="btn_three btn_six btn_hover">Get a Demo</a>
				</div>
			</div>
		</section>
	</div>

<section class="partner_logo_area_five sec_pad">
	<div class="container">
		<h2 class="f_size_30 f_600 text-center t_color l_height45 mb_70">Market leaders use app to nrich <br>their brand & business.</h2>
		<div class="row partner_info">
			<div class="logo_item wow fadeInLeft" data-wow-delay="0.1s">
				<a href="#"><img src="img/home3/logo_01.png" alt=""></a>
			</div>
			<div class="logo_item wow fadeInLeft" data-wow-delay="0.2s">
				<a href="#"><img src="img/home3/logo_02.png" alt=""></a>
			</div>
			<div class="logo_item wow fadeInLeft" data-wow-delay="0.3s">
				<a href="#"><img src="img/home3/logo_03.png" alt=""></a>
			</div>
			<div class="logo_item wow fadeInLeft" data-wow-delay="0.4s">
				<a href="#"><img src="img/home3/logo_04.png" alt=""></a>
			</div>
			<div class="logo_item wow fadeInLeft" data-wow-delay="0.5s">
				<a href="#"><img src="img/home3/logo_05.png" alt=""></a>
			</div>
			<div class="logo_item wow fadeInLeft" data-wow-delay="0.6s">
				<a href="#"><img src="img/home3/logo_03.png" alt=""></a>
			</div>
			<div class="logo_item wow fadeInLeft" data-wow-delay="0.7s">
				<a href="#"><img src="img/home3/logo_04.png" alt=""></a>
			</div>
			<div class="logo_item wow fadeInLeft" data-wow-delay="0.8s">
				<a href="#"><img src="img/home3/logo_05.png" alt=""></a>
			</div>
		</div>
	</div>
</section>
@endsection