<!doctype html>
<html lang="en">


<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="img/favicon.png" type="image/x-icon">
    <title>@yield('title') | Harvest Church </title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css')}} ">
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-selector/css/bootstrap-select.min.css')}}" >
    <!--icon font css-->
    <link rel="stylesheet" href="{{ asset('assets/vendors/themify-icon/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/elagent/style.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/flaticon/flaticon.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/animation/animate.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/owl-carousel/assets/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/magnify-pop/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/nice-select/nice-select.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/scroll/jquery.mCustomScrollbar.min.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/responsive.css')}}">
</head>

<body>
    @include('layouts.header')
    @yield('content')   
    @include('layouts.footer')
</body>


<!-- Mirrored from droitthemes.com/html/saasland/home-company.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 01 Jun 2020 05:24:37 GMT -->
</html>